;;; plantuml-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (plantuml-mode) "plantuml-mode" "plantuml-mode.el"
;;;;;;  (20668 48382))
;;; Generated autoloads from plantuml-mode.el

(autoload 'plantuml-mode "plantuml-mode" "\
Major mode for plantuml.

Shortcuts             Command Name
\\[plantuml-complete-symbol]      `plantuml-complete-symbol'

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("plantuml-mode-pkg.el") (20668 48382 302000))

;;;***

(provide 'plantuml-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; plantuml-mode-autoloads.el ends here
