(require 'package)
;; Add the original Emacs Lisp Package Archive
(add-to-list 'package-archives
             '("elpa" . "http://tromey.com/elpa/"))
;; Add the user-contributed repository
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/"))
(package-initialize)

;; Turn off splash screen
(setq inhibit-splash-screen t)

;; Default backup folder
;; Default backup folder
(setq backup-directory-alist
        `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
        `((".*" ,temporary-file-directory t)))

;; Move to recycle bin when deleting via Emacs
(setq delete-by-moving-to-trash t)

;; Open recent file
(require 'recentf)
(recentf-mode 1)

;; Column position
(setq column-number-mode t)

;; Default font
(set-default-font "Consolas-10")

;; Hide toolbar
(tool-bar-mode -1)

;; Auto complete mode
(require 'auto-complete-config)
(ac-config-default)
(setq ac-delay 0.5) ;; eclipse uses 500ms 
;; configure auto complete to work in slime
; (require 'ac-slime)
; (add-hook 'slime-mode-hook 'set-up-slime-ac)
; (add-hook 'slime-repl-mode-hook 'set-up-slime-ac)
;; configure auto complete with nREPL
(require 'ac-nrepl)
(add-hook 'nrepl-mode-hook 'ac-nrepl-setup)
(add-hook 'nrepl-interaction-mode-hook 'ac-nrepl-setup)
(eval-after-load "auto-complete"
  '(add-to-list 'ac-modes 'nrepl-mode))

;; Column position
(setq column-number-mode t)

;; Line wrapping
(global-visual-line-mode t)

;; Line number
(global-linum-mode t)

;; Tab width to 2
(setq-default indent-tabs-mode nil) ; always replace tabs with spaces
(setq-default tab-width 4) ; set tab width to 4 for all buffers

;; Emacs server
(require 'server)
(unless (server-running-p) (server-start))

;; Hiding hidden files in Emacs dired
(require 'dired-x)
(setq dired-omit-files "^\\...+$")
(add-hook 'dired-mode-hook (lambda () (dired-omit-mode 1)))

;; Enable eldoc in clojure buffers
(add-hook 'nrepl-interaction-mode-hook
          'nrepl-turn-on-eldoc-mode)

;; Stop the error buffer from poping up while working in the REPL buffer
(setq nrepl-popup-stacktraces nil)

;; Make C-c C-z switch to the *nrepl* buffer in the current window
(add-to-list 'same-window-buffer-names "*nrepl*")

;; ido mode
(require 'ido)
(ido-mode t)

;; Yasnippet
(require 'yasnippet)
(yas-global-mode 1)

;; Improving the performance of Emacs's display engine
(setq redisplay-dont-pause t)

;; Hide Control-M
(defun hide-ctrl-M ()
  "Hides the disturbing '^M' showing up in files containing mixed UNIX and DOS line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))
  
;; Load tidy
(autoload 'tidy-buffer "tidy" "Run Tidy HTML parser on current buffer" t)
(autoload 'tidy-parse-config-file "tidy" "Parse the `tidy-config-file'" t)
(autoload 'tidy-save-settings "tidy" "Save settings to `tidy-config-file'" t)
(autoload 'tidy-build-menu  "tidy" "Install an options menu for HTML Tidy." t)  

;; Load PlantUML
(add-to-list 'load-path "~/.emacs.d/manual/")
(load "org-export-blocks-format-plantuml")

;; XML format with Xmllint (need to setup an executable file of xmllint first in the PATH)
(defun xml-format ()
  (interactive)
  (save-excursion
    (shell-command-on-region (mark) (point) "xmllint --format -" (buffer-name) t)
  )
)

